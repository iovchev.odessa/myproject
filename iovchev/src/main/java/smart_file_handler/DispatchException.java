package smart_file_handler;

public class DispatchException extends Exception {
    public DispatchException(String message) {
        super(message);
    }
}

