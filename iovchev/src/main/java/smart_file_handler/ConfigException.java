package smart_file_handler;

public class ConfigException extends Exception {
    public ConfigException(String message) {
        super(message);
    }
}
