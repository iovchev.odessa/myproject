package smart_file_handler;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractParser implements Parser {

    private final List<String> supportedExtensions = new ArrayList<>();

    public AbstractParser(List<String> list) {
        supportedExtensions.addAll(list);

    }

    @Override
    public List<String> getSupportedExtensions() {
        return supportedExtensions;
    }

    @Override
    public boolean isSupportFile(String extension) {
        return getSupportedExtensions().contains(extension);
    }

}
