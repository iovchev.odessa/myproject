package smart_file_handler;

public class ConsoleView implements View {
    @Override
    public void print(String str) {
        System.out.println(str);
    }
}
