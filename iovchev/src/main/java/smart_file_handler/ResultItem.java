package smart_file_handler;

public interface ResultItem {
    String getFormatted();
}
