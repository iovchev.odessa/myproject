package smart_file_handler;

public class XmlResultItem implements ResultItem {
    private final String node;

    public XmlResultItem(String node) {
        this.node = node;
    }

    @Override
    public String getFormatted() {
        return "-> " + node;
    }
}
